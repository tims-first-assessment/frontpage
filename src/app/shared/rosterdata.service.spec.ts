import { TestBed } from '@angular/core/testing';

import { RosterdataService } from './rosterdata.service';

describe('RosterdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RosterdataService = TestBed.get(RosterdataService);
    expect(service).toBeTruthy();
  });
});
