import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RosterdataService {

  private url = '//localhost:8080/items'

  constructor(private http: HttpClient) { }
  public getListItems(): Observable<any> {
    return this.http.get<any>(this.url);
  }

  public createPlayerService(addPlayer: object) {
    return this.http.post('//localhost:8080/create', addPlayer);
  }
}
