import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {RosterdataService} from '../shared/rosterdata.service';

@Component({
  selector: 'app-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.css']
})
export class AddPlayerComponent implements OnInit {
  newPlayer: FormGroup;

  constructor(private playerService: RosterdataService) { }

  ngOnInit() {
    this.newPlayer = new FormGroup({name: new FormControl(),
      number: new FormControl(), position: new FormControl(),
      info: new FormControl()});
  }
  public createPlayer() {
    const Ob = {
      name: this.newPlayer.controls.name.value,
      number: this.newPlayer.controls.number.value,
      position: this.newPlayer.controls.position.value,
      info: this.newPlayer.controls.info.value
    };
    this.playerService.createPlayerService(Ob).subscribe();
  }

}
