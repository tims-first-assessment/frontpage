import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RosterHomeComponent } from './roster-home/roster-home.component';
import { AddPlayerComponent } from './add-player/add-player.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: RosterHomeComponent},
  {path: 'add-a-player', component: AddPlayerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
