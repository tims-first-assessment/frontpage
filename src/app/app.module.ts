import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RosterHomeComponent } from './roster-home/roster-home.component';
import { AddPlayerComponent } from './add-player/add-player.component';

import { RosterdataService } from './shared/rosterdata.service';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RosterHomeComponent,
    AddPlayerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [ReactiveFormsModule],
  providers: [RosterdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
