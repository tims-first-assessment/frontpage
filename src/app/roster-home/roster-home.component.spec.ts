import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RosterHomeComponent } from './roster-home.component';

describe('RosterHomeComponent', () => {
  let component: RosterHomeComponent;
  let fixture: ComponentFixture<RosterHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RosterHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosterHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
