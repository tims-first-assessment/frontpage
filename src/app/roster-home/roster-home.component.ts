import { Component, OnInit } from '@angular/core';
import { RosterdataService} from '../shared/rosterdata.service';

@Component({
  selector: 'app-roster-home',
  templateUrl: './roster-home.component.html',
  styleUrls: ['./roster-home.component.css']
})
export class RosterHomeComponent implements OnInit {

  importDataItems: Array<any>;
  constructor(private data: RosterdataService) { }

  ngOnInit() {
    this.data.getListItems().subscribe(data => { this.importDataItems = data; });
  }
}


